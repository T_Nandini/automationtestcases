﻿Browser("Pune · Stays · Airbnb").Navigate "https://www.airbnb.co.in/" @@ hightlight id_;_919650_;_script infofile_;_ZIP::ssf10.xml_;_
Browser("Pune · Stays · Airbnb").Page("Pune · Stays · Airbnb").WebRadioGroup("refinement_paths[]").Select "/homes" @@ hightlight id_;_10000000_;_script infofile_;_ZIP::ssf11.xml_;_
Browser("Pune · Stays · Airbnb").Page("Pune · Stays · Airbnb").WebElement("Nashik").Click @@ hightlight id_;_10000000_;_script infofile_;_ZIP::ssf12.xml_;_
Browser("Pune · Stays · Airbnb").Page("Pune · Stays · Airbnb").WebButton("Choose Friday, December").Click @@ hightlight id_;_10000000_;_script infofile_;_ZIP::ssf13.xml_;_
Browser("Pune · Stays · Airbnb").Page("Pune · Stays · Airbnb").WebButton("Choose Monday, December").Click @@ hightlight id_;_10000000_;_script infofile_;_ZIP::ssf14.xml_;_
Browser("Pune · Stays · Airbnb").Page("Pune · Stays · Airbnb").WebRadioGroup("flexible-dates-flexible_date_s").Select "1" @@ hightlight id_;_10000000_;_script infofile_;_ZIP::ssf15.xml_;_
Browser("Pune · Stays · Airbnb").Page("Pune · Stays · Airbnb").Check CheckPoint("Holiday Lets, Homes, Experiences & Places - Airbnb_2") @@ hightlight id_;_10000000_;_script infofile_;_ZIP::ssf16.xml_;_
